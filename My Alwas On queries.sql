﻿--Brent Ozar stored procedures:
-- sp_Blitz
-- sp_AskBrent

select @@servername as current_host;
-- было: MASTERSQL2012\PRIMARYSQL2012
-- стало после manual failover: SYNCSQL2012\SYNCSQL2012

SELECT type_desc, port FROM sys.tcp_endpoints;

SELECT * FROM sys.database_mirroring_endpoints
SELECT * FROM sys.tcp_endpoints;
select @@version

-- AlwaysOn Availability Groups Catalog Views (Transact-SQL)
select * from sys.availability_databases_cluster
select * from sys.availability_groups_cluster
select * from sys.availability_group_listener_ip_addresses
select * from sys.availability_read_only_routing_lists
select * from sys.availability_group_listeners
select * from sys.availability_replicas
select * from sys.availability_groups

select * from sys.dm_hadr_auto_page_repair
select * from sys.dm_hadr_cluster_networks
select * from sys.dm_hadr_availability_group_states
-- Одна из самых важных таблиц:
-- возвращает is_failover_ready и recovery_lsn
select * from sys.dm_hadr_database_replica_cluster_states

select * from sys.dm_hadr_availability_replica_cluster_nodes

--Самая важная таблица:
select * from sys.dm_hadr_database_replica_states

select * from sys.dm_hadr_availability_replica_cluster_states
select * from sys.dm_hadr_instance_node_map
select * from sys.dm_hadr_availability_replica_states
select * from sys.dm_hadr_name_id_map
select * from sys.dm_hadr_cluster
select * from sys.dm_tcp_listener_states
select * from sys.dm_hadr_cluster_members


